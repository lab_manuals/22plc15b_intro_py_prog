putta:~/.../Programs$ python3 08AssertExceptDemo.py 
Enter a value for a : 7
Enter a value for b : 6
7 / 6 = 1.1666666666666667

putta:~/.../Programs$ python3 08AssertExceptDemo.py 
Enter a value for a : 0
Enter a value for b : 5
AssertionError: a should be greater than 0

putta:~/.../Programs$ python3 08AssertExceptDemo.py 
Enter a value for a : -3
Enter a value for b : 6
AssertionError: a should be greater than 0

putta:~/.../Programs$ python3 08AssertExceptDemo.py 
Enter a value for a : 6
Enter a value for b : 0
Value of b cannot be zero

