Many enraged psychiatrists are inciting a weary butcher.  The butcher is
weary and tired because he has cut meat and steak and lamb for hours and
weeks.  He does not desire to chant about anything with raving psychiatrists,
but he sings about his gingivectomist, he dreams about a single cosmologist,
he thinks about his dog.  The dog is named Herbert.
		-- Racter, "The Policeman's Beard is Half-Constructed"
